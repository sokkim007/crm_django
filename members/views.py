
from asyncio.windows_events import NULL
from tarfile import NUL
from django.http import HttpResponse
from django.template import loader
from django.views.decorators.csrf import csrf_exempt
from .models import *


def index(request):
    return HttpResponse("Hello world!")

def home(request):
    template = loader.get_template('home/home.html')
    return HttpResponse(template.render())


def master(request):
    template = loader.get_template('layout/master.html')
    return HttpResponse(template.render())


def firstpage(request):
    template = loader.get_template('firsthtml.html')
    return HttpResponse(template.render())

def listLeads(request):
    template = loader.get_template('form/lead/index.html')
    arrval = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14]
    data = lead.objects.all().filter(status=True).order_by('-create_at')
    
    context = {
        "first_name": "Anjaneyulu",
        "last_name": "Batta",
        "address": "Hyderabad, India",
        "arrval":arrval,
        "data":data
    }
    return HttpResponse(template.render(context))


def addlead(request):
    template = loader.get_template('form/lead/addform.html')
    return HttpResponse(template.render())

@csrf_exempt
def storelead(request):
    try:
        if request.method == 'POST':
            add =lead()
            # add.code = 
            add.first_name = request.POST["firstname"]
            add.last_name = request.POST["lastname"]
            add.khmer_name = request.POST["khmername"]
            add.contact = request.POST["contact"]
            add.email = request.POST["email"]
            add.company = request.POST["company"]
            add.gender = request.POST["gender"]
            add.address = request.POST["address"]
            add.industry_type = request.POST["industryType"]
            add.facebook = request.POST["facebook"]
            add.save()
            return HttpResponse('Save Successfully !')
    except Exception as ex:
            # e.message, e.args
            return HttpResponse('Save Unsuccessfully !')


def editlead(request):
    # set edit = ''
    data = lead.objects.all().filter(status=True).order_by('-create_at')
    template = loader.get_template('form/lead/addform.html')
    context = {
        "data":data,
        "update":1
    }
    return HttpResponse(template.render(context))


def listQuote(request):
    # return HttpResponse('hello my world')
    template = loader.get_template('form/quote/index.html')
    arrval = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14]
    context = {
        "arrval":arrval
    }
    return HttpResponse(template.render(context))


def addQuote(request):
    template = loader.get_template('form/quote/addform.html')
    return HttpResponse(template.render())