
from django.urls import path
from . import views

urlpatterns = [
    path('', views.home, name='home'),
    path('home', views.home, name='home'),
    path('htmlpage', views.firstpage, name='htmlpage'),
    path('leads', views.listLeads, name='lead'),
    path('lead/add', views.addlead, name='addlead'),
    path('lead/edit', views.editlead, name='editlead'),
    path('lead/store', views.storelead, name='storelead'),
    path('quotes', views.listQuote, name='quote'),
    path('quote/add', views.addQuote, name='addquote'),
]