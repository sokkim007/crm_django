from locale import currency
# from typing_extensions import Required
from django.db import models
from django.utils.timezone import now

class Members(models.Model):
  firstname = models.CharField(max_length=255)
  lastname = models.CharField(max_length=255)
  
  
class Teacher(models.Model):
    first_name = models.CharField(max_length = 10)
    last_name  = models.CharField(max_length = 10)
    email      = models.EmailField(max_length = 20, unique = True)
    d_o_b      = models.DateTimeField()
    address    = models.CharField(max_length = 200)
    def __str__(self):
        return f'{self.first_name} {self.last_name}'
      
      
class lead(models.Model):
    code       = models.AutoField(primary_key=True)
    first_name = models.CharField(max_length = 20)
    last_name  = models.CharField(max_length = 20)
    khmer_name  = models.CharField(max_length = 20)
    gender      = models.CharField(max_length = 7)
    contact    = models.CharField(max_length = 20, unique = True)
    email      = models.EmailField(max_length = 20, blank = True, null = True)
    create_at     = models.DateTimeField(default=now, editable=False)
    address    = models.CharField(max_length = 255, blank = True, null = True)
    company  = models.CharField(max_length = 50,blank = True, null = True)
    industry_type = models.CharField(max_length= 50,blank = True, null = True)
    facebook = models.CharField(max_length= 50, blank = True, null = True)
    cus_status = models.CharField(max_length= 20, default= 'active')
    status = models.BooleanField(default= True)
    # def __str__(self):
    #     return f'{self.first_name} {self.last_name}'
    
class stock_product(models.Model):
    code         = models.AutoField(primary_key=True)
    name_en      = models.CharField(max_length = 50)
    name_kh      = models.CharField(max_length = 50)
    category     = models.CharField(max_length = 50)
    measurement  = models.CharField(max_length = 50)
    stockqty  = models.IntegerField()  
    create_at     = models.DateTimeField(default=now, editable=False)
    status = models.BooleanField(default= True)
    
class quote(models.Model):
    code      = models.AutoField(primary_key=True)
    lead = models.ForeignKey(lead, on_delete=models.CASCADE)
    acknowladgeby  = models.IntegerField()
    quote_status = models.CharField(max_length= 20, default= 'New')
    grandtotal  = models.IntegerField()  
    currency  = models.IntegerField()  
    create_at     = models.DateTimeField(default=now, editable=False)
    status = models.BooleanField(default= True)
    create_by = models.IntegerField()
    

class quote_detail(models.Model):
    quote      = models.ForeignKey(quote, on_delete=models.CASCADE)
    stock_product = models.ForeignKey(stock_product, on_delete=models.CASCADE)
    unit_price    = models.IntegerField()
    qty           = models.IntegerField()
    discount      = models.IntegerField()
    subtotal      = models.IntegerField()  
    create_at     = models.DateTimeField(default=now, editable=False)
    status = models.BooleanField(default= True)
    
    
